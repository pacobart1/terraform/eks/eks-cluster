#!/bin/bash

# retreive metrics
kubectl get --raw "/apis/metrics.k8s.io/v1beta1/nodes"

# if no metrics available it means metrics-server isn't working