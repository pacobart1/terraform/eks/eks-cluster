#!/bin/bash

for d in */ ; do
    echo "================================================================"
    echo "Running tests in $d"
    cd $d
    ./tests.sh
    cd ../
    echo "Finished running tests in $d"
    echo "================================================================"
done