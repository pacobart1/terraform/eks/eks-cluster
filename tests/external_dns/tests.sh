#!/bin/bash

## deploy kube resources
function k8s_setup {
    echo "deploying k8s resources to create record in route53 via external-dns"
    kubectl apply -f manifest.yaml

    echo "get load balancer dns address"
    MAX_RETRIES=10
    CURRENT_RETRY=0
    while true; do
        K8S_SVC_LB_ADDR=$(kubectl get svc -n external-dns-test external-dns-test -o jsonpath='{.status.loadBalancer.ingress[*].hostname}')
        echo "svc lb address: ${K8S_SVC_LB_ADDR}"
        if [ $CURRENT_RETRY -eq $MAX_RETRIES ]; then
            echo "Max retries reached. TEST FAILED"
            cleanup
            exit 1
        elif [ "$K8S_SVC_LB_ADDR" != "" ]; then
            echo "Address found!"
            break
        else
            echo "No address found. Retrying..."
            CURRENT_RETRY=$((CURRENT_RETRY + 1))
            sleep 5
        fi
    done
}

## route53 record creation validation
function get_route53_records {
    echo "validating record created in route53 via external-dns"
    ROUTE53_ZONE_DOMAIN_NAME=devopseks.lab. #IMPORTANT: trailing dot
    echo "route53 zone name: ${ROUTE53_ZONE_DOMAIN_NAME}"

    # get hosted zone id from domain name
    ROUTE53_ZONE_ID=$(aws route53 list-hosted-zones --query "HostedZones[?Name == '${ROUTE53_ZONE_DOMAIN_NAME}'].Id | [0]" --output text | sed s/"\/hostedzone\/"//)
    echo "route53 zone id: ${ROUTE53_ZONE_ID}"

    # lookup record
    echo "lookup records for external-dns-test.${ROUTE53_ZONE_DOMAIN_NAME}"
    MAX_RETRIES=10
    CURRENT_RETRY=0
    while true; do
        ROUTE53_RECORDS=$(aws route53 list-resource-record-sets --hosted-zone-id "${ROUTE53_ZONE_ID}" --query "ResourceRecordSets[?Name=='external-dns-test.${ROUTE53_ZONE_DOMAIN_NAME}']")
        echo "records: ${ROUTE53_RECORDS}"
        if [ $CURRENT_RETRY -eq $MAX_RETRIES ]; then
            echo "Max retries reached. TEST FAILED"
            cleanup
            exit 1
        elif [ "$ROUTE53_RECORDS" != "[]" ]; then
            echo "Records found!"
            break
        else
            echo "No records found. Retrying..."
            CURRENT_RETRY=$((CURRENT_RETRY + 1))
            sleep 5
        fi
    done

    # retrieve alias target from 'A' record
    echo "lookup AliasTarget for external-dns-test.${ROUTE53_ZONE_DOMAIN_NAME} 'A' record"
    ALIAS_TARGET=$(aws route53 list-resource-record-sets --hosted-zone-id "${ROUTE53_ZONE_ID}" --query "ResourceRecordSets[?Name=='external-dns-test.${ROUTE53_ZONE_DOMAIN_NAME}'].AliasTarget[].DNSName" --output text | sed 's/.$//') # NOTE: need to remove trailing dot
    echo "alias target: ${ALIAS_TARGET}"
}

## cleanup exterything
function cleanup {
    kubectl delete -f manifest.yaml;
}

## run tests
k8s_setup
get_route53_records

## validation
echo "validate route53 and k8s service addresses match"
if [ "$K8S_SVC_LB_ADDR" == "$ALIAS_TARGET" ]; then
    echo "RECORDS MATCH! TEST PASSED"
    cleanup
else
    echo "RECORDS DO NOT MATCH. TEST FAILED"
    cleanup
    exit 1
fi
