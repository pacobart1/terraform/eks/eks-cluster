#!/bin/bash

# retrieve ingress nginx load balancer address
K8S_SVC_LB_ADDR=$(kubectl get svc -n ingress-nginx ingress-nginx-controller -o jsonpath='{.status.loadBalancer.ingress[*].hostname}')
echo "load balancer address: ${K8S_SVC_LB_ADDR}"

# https test bypassing cert validation
HTTP_STATUS_CODE=$(curl -sk -o /dev/null -w "%{http_code}"  https://${K8S_SVC_LB_ADDR})
echo "status code: ${HTTP_STATUS_CODE}"

## validation
echo "validate 404 response from nginx"
if [ "$HTTP_STATUS_CODE" == "404" ]; then
    echo "NGINX working! TEST PASSED"
else
    echo "NGINX not working. TEST FAILED"
    exit 1
fi
