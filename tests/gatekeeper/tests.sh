#!/bin/bash

## deploy kube resources
function k8s_setup {
    echo "deploying k8s resources to test gatekeeper"
    kubectl apply -f namespace.yaml
    kubectl apply -f constraint_template.yaml
    kubectl apply -f constraints.yaml
}

function test_bad_pod {
    echo "deploying bad pod to test gatekeeper constraint"
    echo "expecation: should not be able to deploy pod due to not meeting constraints"
    CMD_OUTPUT=$(kubectl apply -f bad_pod.yaml 2>&1)
    VALIDATION='Error from server (Forbidden): error when creating "bad_pod.yaml": admission webhook "validation.gatekeeper.sh" denied the request: [psp-allow-privilege-escalation-container] Privilege escalation container is not allowed: web'
    echo "kubectl apply output: $CMD_OUTPUT"
    echo "validation  expected: $VALIDATION"
    if [ "$CMD_OUTPUT" == "$VALIDATION" ]; then
      echo "Unable to deploy bad pod. TEST PASSED"
    else
      echo "Able to deploy bad pod. TEST FAILED"
      cleanup
      exit 1
    fi
}

function test_good_pod {
    echo "deploying good pod to test gatekeeper constraint"
    echo "expectation: should be able to deploy pod due to meeting constraints"
    kubectl apply -f good_pod.yaml
    sleep 5 # wait for pod to be ready
    CMD_OUTPUT=$(kubectl get pod -n gatekeeper-test static-web-without-security-context-1-good -o jsonpath='{.status.phase}')
    VALIDATION="Running"
    echo "kubectl output: $CMD_OUTPUT"
    echo "   expectation: $VALIDATION"
    if [ "$CMD_OUTPUT" == "$VALIDATION" ]; then
      echo "Able to deploy good pod. TEST PASSED"
    else
      echo "Unable to deploy good pod. TEST FAILED"
      cleanup
      exit 1
    fi
}

## cleanup exterything
function cleanup {
    kubectl delete -f bad_pod.yaml || true # shouldn't exist unless the test_bad_pod test failed, creating a bad pod.
    kubectl delete -f good_pod.yaml
    kubectl delete -f namespace.yaml
    kubectl delete -f constraints.yaml
    kubectl delete -f constraint_template.yaml
}

## run tests
k8s_setup
test_bad_pod
test_good_pod
cleanup
