#!/bin/bash

ROUTE53_ZONE_DOMAIN_NAME=devopseks.lab. #IMPORTANT: trailing dot

## deploy kube resources
function k8s_setup {
    echo "deploying k8s resources to create record in route53 via external-dns"
    kubectl apply -f manifest.yaml

    echo "get load balancer dns address"
    MAX_RETRIES=10
    CURRENT_RETRY=0
    while true; do
        K8S_SVC_LB_ADDR=$(kubectl get svc -n aws-lb-ctl-test aws-lb-ctl-test -o jsonpath='{.status.loadBalancer.ingress[*].hostname}')
        echo "svc lb address: ${K8S_SVC_LB_ADDR}"
        if [ $CURRENT_RETRY -eq $MAX_RETRIES ]; then
            echo "Max retries reached. TEST FAILED"
            cleanup
            exit 1
        elif [ "$K8S_SVC_LB_ADDR" != "" ]; then
            echo "Address found!"
            break
        else
            echo "No address found. Retrying in 5s..."
            CURRENT_RETRY=$((CURRENT_RETRY + 1))
            sleep 5
        fi
    done
}

## route53 record creation validation
function get_route53_records {
    echo "validating record created in route53 via external-dns"
    echo "route53 zone name: ${ROUTE53_ZONE_DOMAIN_NAME}"

    # get hosted zone id from domain name
    ROUTE53_ZONE_ID=$(aws route53 list-hosted-zones --query "HostedZones[?Name == '${ROUTE53_ZONE_DOMAIN_NAME}'].Id | [0]" --output text | sed s/"\/hostedzone\/"//)
    echo "route53 zone id: ${ROUTE53_ZONE_ID}"

    # lookup record
    echo "lookup records for aws-lb-ctl-test.${ROUTE53_ZONE_DOMAIN_NAME}"
    MAX_RETRIES=10
    CURRENT_RETRY=0
    while true; do
        ROUTE53_RECORDS=$(aws route53 list-resource-record-sets --hosted-zone-id "${ROUTE53_ZONE_ID}" --query "ResourceRecordSets[?Name=='aws-lb-ctl-test.${ROUTE53_ZONE_DOMAIN_NAME}']")
        echo "records: ${ROUTE53_RECORDS}"
        if [ $CURRENT_RETRY -eq $MAX_RETRIES ]; then
            echo "Max retries reached. TEST FAILED"
            cleanup
            exit 1
        elif [ "$ROUTE53_RECORDS" != "[]" ]; then
            echo "Records found!"
            break
        else
            echo "No records found. Retrying in 5s..."
            CURRENT_RETRY=$((CURRENT_RETRY + 1))
            sleep 5
        fi
    done

    # retrieve alias target from 'A' record
    echo "lookup AliasTarget for aws-lb-ctl-test.${ROUTE53_ZONE_DOMAIN_NAME} 'A' record"
    ALIAS_TARGET=$(aws route53 list-resource-record-sets --hosted-zone-id "${ROUTE53_ZONE_ID}" --query "ResourceRecordSets[?Name=='aws-lb-ctl-test.${ROUTE53_ZONE_DOMAIN_NAME}'].AliasTarget[].DNSName" --output text | sed 's/.$//') # NOTE: need to remove trailing dot
    echo "alias target: ${ALIAS_TARGET}"
}

## cleanup exterything
function cleanup {
    echo "cleaning up resources"
    kubectl delete -f manifest.yaml;
}

function test_nlb {
    echo "test alb creation and app accessible"
    MAX_RETRIES=50
    CURRENT_RETRY=0
    while true; do
        LB_STATUS=$(aws elbv2 describe-load-balancers --names aws-lb-ctl-test --query "LoadBalancers[*].State.Code" --output text)
        echo "Load balancer status: $LB_STATUS"
        if [ $CURRENT_RETRY -eq $MAX_RETRIES ]; then
            echo "Max retries reached. TEST FAILED"
            cleanup
            exit 1
        elif [ "$LB_STATUS" == "active" ]; then
            echo "Load Balancer Active"
            break
        else
            echo "No address found. Retrying in 5s..."
            CURRENT_RETRY=$((CURRENT_RETRY + 1))
            sleep 5
        fi
    done


    LB_ARN=$(aws elbv2 describe-load-balancers --names aws-lb-ctl-test --query "LoadBalancers[*].LoadBalancerArn" --output text)
    TG_ARN=$(aws elbv2 describe-target-groups --load-balancer-arn $LB_ARN --query "TargetGroups[*].TargetGroupArn" --output text)

    # get target group members. will take time to become healthy.
    echo "waiting for target group member to be healthy"
    MAX_RETRIES=50
    CURRENT_RETRY=0
    while true; do
        TG_MEMBER_STATUS=$(aws elbv2 describe-target-health --target-group-arn $TG_ARN --query "TargetHealthDescriptions[*].TargetHealth.State" --output text)
        echo "target group member status: $TG_MEMBER_STATUS"
        if [ $CURRENT_RETRY -eq $MAX_RETRIES ]; then
            echo "Max retries reached. TEST FAILED"
            cleanup
            exit 1
        elif [ "$TG_MEMBER_STATUS" == "healthy" ]; then
            echo "target group member healthy"
            break
        else
            echo "target group member not healthy. Retrying in 5s..."
            CURRENT_RETRY=$((CURRENT_RETRY + 1))
            sleep 5
        fi
    done

    # testing end to end connectivity. Need to use ELB address as the domain isn't accessible via the internet.
    MAX_RETRIES=50
    CURRENT_RETRY=0
    while true; do
        echo "using address: $K8S_SVC_LB_ADDR with host header: network-policy-test.$ROUTE53_ZONE_DOMAIN_NAME"
        STATUS_CODE=$(curl -H "Host: network-policy-test.$ROUTE53_ZONE_DOMAIN_NAME" --connect-timeout 3 -sk -o /dev/null -w "%{http_code}" http://$K8S_SVC_LB_ADDR)
        echo "status code: $STATUS_CODE"
        if [ $CURRENT_RETRY -eq $MAX_RETRIES ]; then
            echo "Max retries reached. TEST FAILED"
            cleanup
            exit 1
        elif [ "$STATUS_CODE" == "200" ]; then
            echo "Connected to pod successfully. TEST PASSED."
            break
        elif [ "$STATUS_CODE" == "000" ]; then
            echo "trying connection again."
            CURRENT_RETRY=$((CURRENT_RETRY + 1))
        else
          echo "Unable to connect to pod. TEST FAILED."
          cleanup
          exit 1
        fi
    done
}

## run tests
k8s_setup
get_route53_records
test_nlb
cleanup
