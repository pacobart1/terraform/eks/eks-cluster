#!/bin/bash

DOMAIN_NAME="devopseks.lab"
## deploy kube resources
function k8s_setup {
    echo "deploying k8s resources to test network policies"
    kubectl apply -f manifest.yaml
}

## cleanup exterything
function cleanup {
    echo "cleaning up resources"
    kubectl delete -f network_policy_allow.yaml
    kubectl delete -f manifest.yaml
}

function test_unsuccessful_connection {
    echo "testing unsuccessful connection to pod with default deny network policy enabled."
    MAX_RETRIES=10
    CURRENT_RETRY=0
    while true; do
        K8S_ING_LB_ADDR=$(kubectl get ing -n network-policy-test network-policy-test-ingress -o jsonpath='{.status.loadBalancer.ingress[*].hostname}')
        echo "ingress hostname: $K8S_ING_LB_ADDR"
        if [ $CURRENT_RETRY -eq $MAX_RETRIES ]; then
            echo "Max retries reached. TEST FAILED"
            cleanup
            exit 1
        elif [ "$K8S_ING_LB_ADDR" != "" ]; then
            echo "Address found!"
            break
        else
            echo "No adress found. Retrying in 5s..."
            CURRENT_RETRY=$((CURRENT_RETRY + 1))
            sleep 5
        fi
    done
    echo "using address: $K8S_ING_LB_ADDR with host header: network-policy-test.$DOMAIN_NAME"
    STATUS_CODE=$(curl -H "Host: network-policy-test.$DOMAIN_NAME" --connect-timeout 3 -sk -o /dev/null -w "%{http_code}" http://$K8S_ING_LB_ADDR)
    echo "status code: $STATUS_CODE"
    if [ "$STATUS_CODE" == "200" ]; then
        echo "Able to connect to pod. TEST FAILED."
        cleanup
      exit 1
    else
      echo "Unable to connect to pod successfully. TEST PASSED."
    fi
}

function test_successful_connection {
    echo "testing successful connection to pod with allow traffic network policy enabled."
    kubectl apply -f network_policy_allow.yaml
    MAX_RETRIES=10
    CURRENT_RETRY=0
    while true; do
        K8S_ING_LB_ADDR=$(kubectl get ing -n network-policy-test network-policy-test-ingress -o jsonpath='{.status.loadBalancer.ingress[*].hostname}')
        echo "ingress hostname: $K8S_ING_LB_ADDR"
        if [ $CURRENT_RETRY -eq $MAX_RETRIES ]; then
            echo "Max retries reached. TEST FAILED"
            cleanup
            exit 1
        elif [ "$K8S_ING_LB_ADDR" != "" ]; then
            echo "Address found!"
            break
        else
            echo "No adress found. Retrying in 5s..."
            CURRENT_RETRY=$((CURRENT_RETRY + 1))
            sleep 5
        fi
    done
    
    echo "using address: $K8S_ING_LB_ADDR with host header: network-policy-test.$DOMAIN_NAME"
    MAX_RETRIES=10
    CURRENT_RETRY=0
    while true; do
    STATUS_CODE=$(curl -H "Host: network-policy-test.$DOMAIN_NAME" --connect-timeout 3 -sk -o /dev/null -w "%{http_code}" https://$K8S_ING_LB_ADDR)
        echo "status code: $STATUS_CODE"
        if [ $CURRENT_RETRY -eq $MAX_RETRIES ]; then
            echo "Max retries reached. TEST FAILED"
            cleanup
            exit 1
        elif [ "$STATUS_CODE" == "200" ]; then
            echo "Connected to pod successfully. TEST PASSED."
            break
        elif [ "$STATUS_CODE" == "000" ]; then
          echo "HTTP status code error"
          CURRENT_RETRY=$((CURRENT_RETRY + 1))
          sleep 5
        else
            echo "Unable to connect to pod. TEST FAILED."
            cleanup
            exit 1
        fi
    done
}

## run tests
k8s_setup
test_unsuccessful_connection
test_successful_connection
cleanup