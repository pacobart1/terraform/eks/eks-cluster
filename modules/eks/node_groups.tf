################################################################################
# EKS Managed Node Group
################################################################################

module "eks_managed_node_group" {
  source = "./modules/eks-managed-node-group"

  for_each = { for k, v in local.managed_node_groups : k => v }

  create = try(each.value.create, true)

  cluster_name      = module.eks.cluster_name
  cluster_version   = try(each.value.cluster_version, local.managed_node_group_defaults.cluster_version, var.eks_cluster_version)
  cluster_ip_family = var.cluster_ip_family

  # EKS Managed Node Group
  name            = try(each.value.name, "${module.eks.cluster_name}-${each.key}")
  use_name_prefix = try(each.value.use_name_prefix, local.managed_node_group_defaults.use_name_prefix, true)

  subnet_ids = try(each.value.subnet_ids, local.managed_node_group_defaults.subnet_ids, var.worker_subnet_ids)

  min_size     = try(each.value.min_size, local.managed_node_group_defaults.min_size, 1)
  max_size     = try(each.value.max_size, local.managed_node_group_defaults.max_size, 3)
  desired_size = try(each.value.desired_size, local.managed_node_group_defaults.desired_size, 1)

  ami_id              = try(each.value.ami_id, local.managed_node_group_defaults.ami_id, "")
  ami_type            = try(each.value.ami_type, local.managed_node_group_defaults.ami_type, null)
  ami_release_version = try(each.value.ami_release_version, local.managed_node_group_defaults.ami_release_version, null)

  capacity_type        = try(each.value.capacity_type, local.managed_node_group_defaults.capacity_type, null)
  disk_size            = try(each.value.disk_size, local.managed_node_group_defaults.disk_size, null)
  force_update_version = try(each.value.force_update_version, local.managed_node_group_defaults.force_update_version, null)
  instance_types       = try(each.value.instance_types, local.managed_node_group_defaults.instance_types, null)
  labels               = try(each.value.labels, local.managed_node_group_defaults.labels, null)

  remote_access = try(each.value.remote_access, local.managed_node_group_defaults.remote_access, {})
  taints        = try(each.value.taints, local.managed_node_group_defaults.taints, {})
  update_config = try(each.value.update_config, local.managed_node_group_defaults.update_config, null)
  timeouts      = try(each.value.timeouts, local.managed_node_group_defaults.timeouts, {})

  # User data
  platform                   = try(each.value.platform, local.managed_node_group_defaults.platform, "linux")
  cluster_endpoint           = try(module.eks.cluster_endpoint, "")
  cluster_auth_base64        = try(module.eks.cluster_certificate_authority_data, "")
  cluster_service_ipv4_cidr  = var.cluster_service_ipv4_cidr
  enable_bootstrap_user_data = try(each.value.enable_bootstrap_user_data, local.managed_node_group_defaults.enable_bootstrap_user_data, false)
  pre_bootstrap_user_data    = try(each.value.pre_bootstrap_user_data, local.managed_node_group_defaults.pre_bootstrap_user_data, "")
  post_bootstrap_user_data   = try(each.value.post_bootstrap_user_data, local.managed_node_group_defaults.post_bootstrap_user_data, "")
  bootstrap_extra_args       = try(each.value.bootstrap_extra_args, local.managed_node_group_defaults.bootstrap_extra_args, "")
  user_data_template_path    = try(each.value.user_data_template_path, local.managed_node_group_defaults.user_data_template_path, "")

  # Launch Template
  create_launch_template                 = try(each.value.create_launch_template, local.managed_node_group_defaults.create_launch_template, true)
  use_custom_launch_template             = try(each.value.use_custom_launch_template, local.managed_node_group_defaults.use_custom_launch_template, true)
  launch_template_id                     = try(each.value.launch_template_id, local.managed_node_group_defaults.launch_template_id, "")
  launch_template_name                   = try(each.value.launch_template_name, local.managed_node_group_defaults.launch_template_name, each.key)
  launch_template_use_name_prefix        = try(each.value.launch_template_use_name_prefix, local.managed_node_group_defaults.launch_template_use_name_prefix, true)
  launch_template_version                = try(each.value.launch_template_version, local.managed_node_group_defaults.launch_template_version, null)
  launch_template_default_version        = try(each.value.launch_template_default_version, local.managed_node_group_defaults.launch_template_default_version, null)
  update_launch_template_default_version = try(each.value.update_launch_template_default_version, local.managed_node_group_defaults.update_launch_template_default_version, true)
  launch_template_description            = try(each.value.launch_template_description, local.managed_node_group_defaults.launch_template_description, "Custom launch template for ${try(each.value.name, each.key)} EKS managed node group")
  launch_template_tags                   = try(each.value.launch_template_tags, local.managed_node_group_defaults.launch_template_tags, {})
  tag_specifications                     = try(each.value.tag_specifications, local.managed_node_group_defaults.tag_specifications, ["instance", "volume", "network-interface"])

  ebs_optimized           = try(each.value.ebs_optimized, local.managed_node_group_defaults.ebs_optimized, null)
  key_name                = try(each.value.key_name, local.managed_node_group_defaults.key_name, null)
  disable_api_termination = try(each.value.disable_api_termination, local.managed_node_group_defaults.disable_api_termination, null)
  kernel_id               = try(each.value.kernel_id, local.managed_node_group_defaults.kernel_id, null)
  ram_disk_id             = try(each.value.ram_disk_id, local.managed_node_group_defaults.ram_disk_id, null)

  block_device_mappings              = try(each.value.block_device_mappings, local.managed_node_group_defaults.block_device_mappings, {})
  capacity_reservation_specification = try(each.value.capacity_reservation_specification, local.managed_node_group_defaults.capacity_reservation_specification, {})
  cpu_options                        = try(each.value.cpu_options, local.managed_node_group_defaults.cpu_options, {})
  credit_specification               = try(each.value.credit_specification, local.managed_node_group_defaults.credit_specification, {})
  elastic_gpu_specifications         = try(each.value.elastic_gpu_specifications, local.managed_node_group_defaults.elastic_gpu_specifications, {})
  elastic_inference_accelerator      = try(each.value.elastic_inference_accelerator, local.managed_node_group_defaults.elastic_inference_accelerator, {})
  enclave_options                    = try(each.value.enclave_options, local.managed_node_group_defaults.enclave_options, {})
  instance_market_options            = try(each.value.instance_market_options, local.managed_node_group_defaults.instance_market_options, {})
  license_specifications             = try(each.value.license_specifications, local.managed_node_group_defaults.license_specifications, {})
  metadata_options                   = try(each.value.metadata_options, local.managed_node_group_defaults.metadata_options, null)
  enable_monitoring                  = try(each.value.enable_monitoring, local.managed_node_group_defaults.enable_monitoring, true)
  network_interfaces                 = try(each.value.network_interfaces, local.managed_node_group_defaults.network_interfaces, [])
  placement                          = try(each.value.placement, local.managed_node_group_defaults.placement, {})
  maintenance_options                = try(each.value.maintenance_options, local.managed_node_group_defaults.maintenance_options, {})
  private_dns_name_options           = try(each.value.private_dns_name_options, local.managed_node_group_defaults.private_dns_name_options, {})

  # IAM role
  create_iam_role               = try(each.value.create_iam_role, local.managed_node_group_defaults.create_iam_role, true)
  iam_role_arn                  = try(each.value.iam_role_arn, local.managed_node_group_defaults.iam_role_arn, null)
  iam_role_name                 = "${module.eks.cluster_name}-${each.key}"
  iam_role_use_name_prefix      = try(each.value.iam_role_use_name_prefix, local.managed_node_group_defaults.iam_role_use_name_prefix, false)
  iam_role_path                 = try(each.value.iam_role_path, local.managed_node_group_defaults.iam_role_path, null)
  iam_role_description          = try(each.value.iam_role_description, local.managed_node_group_defaults.iam_role_description, "EKS managed node group IAM role")
  iam_role_permissions_boundary = try(each.value.iam_role_permissions_boundary, local.managed_node_group_defaults.iam_role_permissions_boundary, null)
  iam_role_tags                 = try(each.value.iam_role_tags, local.managed_node_group_defaults.iam_role_tags, {})
  iam_role_attach_cni_policy    = try(each.value.iam_role_attach_cni_policy, local.managed_node_group_defaults.iam_role_attach_cni_policy, true)
  # To better understand why this `lookup()` logic is required, see:
  # https://github.com/hashicorp/terraform/issues/31646#issuecomment-1217279031
  iam_role_additional_policies = lookup(each.value, "iam_role_additional_policies", lookup(local.managed_node_group_defaults, "iam_role_additional_policies", {}))

  create_schedule = try(each.value.create_schedule, local.managed_node_group_defaults.create_schedule, true)
  schedules       = try(each.value.schedules, local.managed_node_group_defaults.schedules, {})

  # Security group
  vpc_security_group_ids            = compact(concat([module.eks.node_security_group_id, ], try(each.value.vpc_security_group_ids, local.managed_node_group_defaults.vpc_security_group_ids, [])))
  cluster_primary_security_group_id = try(each.value.attach_cluster_primary_security_group, local.managed_node_group_defaults.attach_cluster_primary_security_group, false) ? module.eks.cluster_primary_security_group_id : null

  tags = merge(var.tags, try(each.value.tags, local.managed_node_group_defaults.tags, {}))

  depends_on = [
    kubernetes_config_map.aws_auth,
    kubectl_manifest.eni_config,
  ]
}
