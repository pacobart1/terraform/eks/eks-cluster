# NOTE: detector is enabled once per AWS Account. Use data block if GuardDuty was enabled via another mechanism.
#resource "aws_guardduty_detector" "detector" {
#  enable = true
#}

data "aws_guardduty_detector" "detector" {}

resource "aws_guardduty_detector_feature" "eks_runtime" {
  detector_id = data.aws_guardduty_detector.detector.id
  name        = "EKS_RUNTIME_MONITORING"
  status      = "ENABLED"

  additional_configuration {
    name   = "EKS_ADDON_MANAGEMENT"
    status = "ENABLED"
  }
}

resource "aws_guardduty_detector_feature" "eks_audit" {
  detector_id = data.aws_guardduty_detector.detector.id
  name        = "EKS_AUDIT_LOGS"
  status      = "ENABLED"
}
