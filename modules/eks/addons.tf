# NOTE: addons needing to be installed first, post node group
module "eks_managed_addons" {
  source          = "./modules/eks_addons"
  cluster_name    = module.eks.cluster_name
  cluster_version = module.eks.cluster_version
  eks_addons = {
    coredns = {}
    #aws-ebs-csi-driver = {
    #  service_account_role_arn = module.aws_ebs_csi_driver_pod_identity[count.index].iam_role_arn
    #}
    #eks-pod-identity-agent = {}
    #aws-guardduty-agent    = {}
    # disable adot for now until cert manager is fixed
    #adot = {
    #  service_account_role_arn = module.adot_collector_irsa[count.index].iam_role_arn
    #}
    # below are installed prior to compute.
    #vpc-cni    = {}
    #kube-proxy = {}
  }

  tags = var.tags
  depends_on = [
    module.eks_managed_node_group,
  ]
}

#module "eks_blueprints_addons_core" {
#  #checkov:skip=CKV_TF_1:modules are pinned to version in terraform cloud
#  source  = "aws-ia/eks-blueprints-addons/aws"
#  version = "1.12.0"
#  count   = var.enable_addons ? 1 : 0
#
#  cluster_name      = module.eks.cluster_name
#  cluster_endpoint  = module.eks.cluster_endpoint
#  cluster_version   = module.eks.cluster_version
#  oidc_provider_arn = module.eks.oidc_provider_arn
#
#  # EKS Add-on
#  eks_addons = {
#    coredns = {}
#    #aws-ebs-csi-driver = {
#    #  service_account_role_arn = module.aws_ebs_csi_driver_pod_identity[count.index].iam_role_arn
#    #}
#    eks-pod-identity-agent = {}
#    aws-guardduty-agent    = {}
#    # disable adot for now until cert manager is fixed
#    #adot = {
#    #  service_account_role_arn = module.adot_collector_irsa[count.index].iam_role_arn
#    #}
#    # below are installed prior to compute.
#    #vpc-cni    = {}
#    #kube-proxy = {}
#  }
#
#  tags = var.tags
#}

# NOTE: deploy addons if enable_argocd = false. otherwise argo will deploy all of these
#module "eks_blueprints_addons" {
#  #checkov:skip=CKV_TF_1:modules are pinned to version in terraform cloud
#  source  = "aws-ia/eks-blueprints-addons/aws"
#  version = "1.9.2"
#  count   = var.enable_addons && !var.enable_argocd ? 1 : 0
#
#  cluster_name      = module.eks.cluster_name
#  cluster_endpoint  = module.eks.cluster_endpoint
#  cluster_version   = module.eks.cluster_version
#  oidc_provider_arn = module.eks.oidc_provider_arn
#
#  # Add-ons
#  enable_external_dns            = true
#  external_dns_route53_zone_arns = [aws_route53_zone.public.arn]
#  external_dns = {
#    values = [
#      jsonencode({
#        provider      = "aws"
#        zoneIdFilters = [aws_route53_zone.public.zone_id]
#        policy        = "sync"
#        txtOwnerId    = module.eks.cluster_name
#        serviceMonitor = {
#          enabled = true
#        }
#      })
#    ]
#  }
#  enable_aws_load_balancer_controller = true
#  enable_ingress_nginx                = true
#  ingress_nginx = {
#    values = [
#      jsonencode({
#        controller = {
#          replicaCount = 3
#          autoscaling = {
#            enabled                           = true
#            minReplicas                       = 3
#            maxReplicas                       = 10
#            targetCPUUtilizationPercentage    = 50
#            targetMemoryUtilizationPercentage = 70
#            scaleDown = {
#              stablizationWindowSeconds = 300
#              policies = [
#                {
#                  type          = "Pods"
#                  value         = 1
#                  periodSeconds = 180
#                }
#              ]
#            }
#            scaleUp = {
#              stablizationWindowSeconds = 300
#              policies = [
#                {
#                  type          = "Pods"
#                  value         = 2
#                  periodSeconds = 60
#                }
#              ]
#            }
#          }
#          resources = {
#            requests = {
#              cpu    = "100m"
#              memory = "90Mi"
#            }
#            limits = {
#              cpu    = "125m"
#              memory = "128Mi"
#            }
#          }
#          service = {
#            annotations = {
#              "service.beta.kubernetes.io/aws-load-balancer-name"                              = substr("${module.eks.cluster_name}-ing-nginx", 0, 32)
#              "service.beta.kubernetes.io/aws-load-balancer-type"                              = "external"
#              "service.beta.kubernetes.io/aws-load-balancer-nlb-target-type"                   = "ip"
#              "service.beta.kubernetes.io/aws-load-balancer-scheme"                            = "internet-facing"
#              "service.beta.kubernetes.io/aws-load-balancer-cross-zone-load-balancing-enabled" = true
#            }
#          }
#          service = {
#            annotations = {
#              "service.beta.kubernetes.io/aws-load-balancer-name"                              = substr("${module.eks.cluster_name}-ing-nginx", 0, 32)
#              "service.beta.kubernetes.io/aws-load-balancer-type"                              = "external"
#              "service.beta.kubernetes.io/aws-load-balancer-nlb-target-type"                   = "ip"
#              "service.beta.kubernetes.io/aws-load-balancer-scheme"                            = "internal"
#              "service.beta.kubernetes.io/aws-load-balancer-cross-zone-load-balancing-enabled" = true
#            }
#          }
#        }
#      })
#    ]
#  }
#  enable_cert_manager = true
#  enable_karpenter    = true
#  enable_gatekeeper   = true
#
#  #observability
#  enable_kube_prometheus_stack = true
#  enable_aws_for_fluentbit     = true
#  aws_for_fluentbit = {
#    enable_containerinsights = true
#  }
#  enable_metrics_server = true
#
#  helm_releases = {
#    adot-collector = {
#      name          = "adot-collector"
#      chart         = "adot-exporter-for-eks-on-ec2"
#      chart_version = "0.19.0"
#      repository    = "https://aws-observability.github.io/aws-otel-helm-charts"
#      values = [
#        jsonencode({
#          awsRegion   = var.aws_region
#          clusterName = module.eks.cluster_name
#          serviceAccount = {
#            annotations = {
#              "eks.amazonaws.com/role-arn" = module.adot_collector_irsa[count.index].iam_role_arn
#            }
#          }
#          adotCollector = {
#            daemonSet = {
#              serviceAccount = {
#                annotations = {
#                  "eks.amazonaws.com/role-arn" = module.adot_collector_irsa[count.index].iam_role_arn
#                }
#              }
#              extensions = {
#                sigv4auth = {
#                  region = var.aws_region
#                }
#              }
#              cwexporters = {
#                logGroupName = "" # TODO add log group name
#              }
#              ampexporters = {
#                endpoint = "" # TODO: add AMP
#              }
#              service = {
#                metrics = {
#                  receivers = ["awscontainerinsightreceiver"]
#                  exporters = ["awsemf"]
#                }
#              }
#            }
#            sidecar = {
#              regionS3 = var.aws_region
#            }
#          }
#        })
#      ]
#    }
#  }
#
#  tags = var.tags
#
#  depends_on = [
#    module.eks_blueprints_addons_core
#  ]
#}
#