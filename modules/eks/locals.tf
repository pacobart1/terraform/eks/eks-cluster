locals {
  name           = var.name
  region         = var.aws_region
  aws_account_id = data.aws_caller_identity.current.account_id
  dns_suffix     = data.aws_partition.current.dns_suffix
  partition      = data.aws_partition.current.partition

  #azs      = slice(data.aws_availability_zones.available.names, 0, 3)

  eks_endpoint                       = module.eks.cluster_endpoint
  cluster_certificate_authority_data = module.eks.cluster_certificate_authority_data
  cluster_ca_certificate             = base64decode(local.cluster_certificate_authority_data)
  eks_token                          = data.aws_eks_cluster_auth.this.token

  managed_node_group_defaults = merge({
    use_name_prefix = true
    iam_role_additional_policies = {
      ssm_managed_instance = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore",
    }
    ami_type       = "AL2_x86_64"
    instance_types = ["m6i.large", "m5.large", "m5n.large", "m5zn.large"]
    # NOTE: we will use karpenter so these values will not need to be updated. They are set this way to help with errors if additional AZ's are added
    min_size     = 1
    max_size     = 10
    desired_size = length(var.availability_zones)
    metadata_options = {
      http_endpoint               = "enabled"
      http_tokens                 = "required"
      http_put_response_hop_limit = 2
    }
    force_update_version = true
    update_config = {
      max_unavailable_percentage = 33
    }
  }, var.eks_managed_node_group_defaults)

  managed_node_groups = {
    for k, v in var.eks_managed_node_groups : k => {
      instance_types = try(v.instance_types, null)
      ami_id         = try(v.custom_ami_id, null)
      labels         = try(v.k8s_labels, {})
      taints         = try(v.k8s_taints, {})
      tags = merge(
        var.tags,
        try(v.tags, {}),
        {
          "Name" = "${local.name}-${k}"
      })
    }
  }

  tolerations_from_taints = flatten([
    for k, v in var.eks_managed_node_groups : [
      for taint in v.k8s_taints : {
        key      = taint.key
        value    = taint.value
        effect   = "NoSchedule" # TODO: if other contidtions exist in the future will add here.
        operator = "Equal"
      }
    ]
  ])

  fargate_profiles = {}

  # Managed node IAM Roles for aws-auth
  managed_node_group_aws_auth_config_map = length(local.managed_node_groups) > 0 == true ? [
    for key, node in local.managed_node_groups : {
      rolearn : try(node.iam_role_arn, "arn:aws:iam::${local.aws_account_id}:role/${module.eks.cluster_name}-${key}")
      username : "system:node:{{EC2PrivateDNSName}}"
      groups : [
        "system:bootstrappers",
        "system:nodes"
      ]
    }
  ] : []
  # Fargate node IAM Roles for aws-auth
  fargate_profiles_aws_auth_config_map = length(local.fargate_profiles) > 0 ? [
    for key, node in local.fargate_profiles : {
      rolearn : try(node.iam_role_arn, "arn:aws:iam::${local.aws_account_id}:role/${module.eks.cluster_name}-${node.fargate_profile_name}")
      username : "system:node:{{SessionName}}"
      groups : [
        "system:bootstrappers",
        "system:nodes",
        "system:node-proxier"
      ]
    }
  ] : []

  roles_aws_auth_config_map = [
    {
      rolearn  = "arn:aws:sts::${local.aws_account_id}:role/automation_pipeline_role"
      username = "automation"
      groups   = ["system:masters"]
    },
    {
      rolearn  = data.aws_caller_identity.current.arn
      username = "deployer"
      groups   = ["system:masters"]
    },
    {
      rolearn  = "arn:aws:iam::${local.aws_account_id}:role/Admin",
      username = "AdminRole"
      groups   = ["system:masters"]
    },
  ]
}
