output "cluster_name" {
  value = module.eks.cluster_name
}

output "cluster_endpoint" {
  value = module.eks.cluster_endpoint
}

output "cluster_certificate_authority_data" {
  value = module.eks.cluster_certificate_authority_data
}

#output "argo_application_manifest" {
#  value = var.enable_addons && var.enable_argocd ? module.argocd[0].argo_application_manifest : ""
#}
#
#output "argocd_domain_name" {
#  value = var.enable_addons && var.enable_argocd ? module.argocd[0].argocd_domain_name : ""
#}
#
#output "argocd_secretsmanager_secret" {
#  value = var.enable_addons && var.enable_argocd ? module.argocd[0].argocd_secretsmanager_secret : ""
#}

output "k8s_tolerations" {
  value = local.tolerations_from_taints
}