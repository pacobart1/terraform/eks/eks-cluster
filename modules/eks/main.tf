module "eks" {
  #checkov:skip=CKV_TF_1:modules are pinned to version in terraform cloud
  source  = "terraform-aws-modules/eks/aws"
  version = "~> 19.21.0"

  cluster_name                         = local.name
  cluster_version                      = var.eks_cluster_version
  cluster_endpoint_public_access       = var.cluster_endpoint_public_access
  cluster_endpoint_public_access_cidrs = var.cluster_endpoint_public_access_cidrs
  cluster_endpoint_private_access      = var.cluster_endpoint_private_access
  kms_key_administrators               = var.kms_key_administrators

  cluster_addons = {
    kube-proxy = {}
    vpc-cni = {
      # Specify the VPC CNI addon should be deployed before compute to ensure
      before_compute           = true
      most_recent              = true # To ensure access to the latest settings provided
      service_account_role_arn = module.vpc_cni_irsa.iam_role_arn
      configuration_values = jsonencode({
        enableNetworkPolicy = "true" # NOTE: built in Network Policy support. Requires EKS 1.25 or higher. Removes need for calico/cilium
        #nodeAgent = {
        #  enableCloudWatchLogs = "true"
        #}
        env = {
          # Reference https://aws.github.io/aws-eks-best-practices/reliability/docs/networkmanagement/#cni-custom-networking
          AWS_VPC_K8S_CNI_CUSTOM_NETWORK_CFG = "true"
          ENI_CONFIG_LABEL_DEF               = "topology.kubernetes.io/zone"
          ANNOTATE_POD_IP                    = "true"

          # Reference docs https://docs.aws.amazon.com/eks/latest/userguide/cni-increase-ip-addresses.html
          ENABLE_PREFIX_DELEGATION = "true"
          WARM_PREFIX_TARGET       = "1"
        }
      })
    }
    #coredns                = {} # NOTE: coredns needs to be deployed after the node group due to needing nodes to run the pods before it will successfully complete.
    eks-pod-identity-agent = {}
    aws-guardduty-agent    = {}
    aws-ebs-csi-driver = {
      service_account_role_arn = module.aws_ebs_csi_driver_pod_identity[0].iam_role_arn
    }
    aws-efs-csi-driver = {
      service_account_role_arn = module.aws_efs_csi_driver_pod_identity[0].iam_role_arn
      configuration_values = jsonencode({
        deleteAccessPointRootDir = true
      })
    }
  }

  vpc_id                   = var.vpc_id
  subnet_ids               = var.worker_subnet_ids
  control_plane_subnet_ids = var.control_plane_subnet_ids

  # NOTE: managing the aws-auth configmap doesn't work well and causes issues with single-touch deployments. Separating this logic out into separate module works better. See below.
  #create_aws_auth_configmap = true # NOTE: when using managed node groups, the aws-auth configmap is automatically created and applied for your cluster
  #manage_aws_auth_configmap = true
  #aws_auth_roles = concat([
  #  {
  #    rolearn  = data.aws_caller_identity.current.arn
  #    username = "deployer"
  #    groups   = ["system:masters"]
  #  },
  #], var.additional_aws_auth_roles)

  #eks_managed_node_group_defaults = {
  #  use_name_prefix = true
  #  iam_role_additional_policies = {
  #    ssm_managed_instance = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore",
  #  }
  #  ami_type       = "AL2_x86_64"
  #  instance_types = ["m6i.large", "m5.large", "m5n.large", "m5zn.large"]
  #  # NOTE: we will use karpenter so these values will not need to be updated. They are set this way to help with errors if additional AZ's are added
  #  min_size     = 1
  #  max_size     = 10
  #  desired_size = length(var.availability_zones)
  #  metadata_options = {
  #    http_endpoint               = "enabled"
  #    http_tokens                 = "required"
  #    http_put_response_hop_limit = 2
  #  }
  #  force_update_version = true
  #}

  #eks_managed_node_groups              = local.managed_node_groups
  node_security_group_additional_rules = {}
  tags                                 = var.tags
}

#################################################################################
## aws-auth configmap
#################################################################################

resource "kubernetes_config_map" "aws_auth" {
  count = 1 # TODO: when terraform supports new authentication mechanism will switch to make this optional

  metadata {
    name      = "aws-auth"
    namespace = "kube-system"

  }

  data = {
    mapRoles = yamlencode(
      distinct(concat(
        local.managed_node_group_aws_auth_config_map,
        [{
          rolearn  = data.aws_caller_identity.current.arn
          username = "deployer"
          groups   = ["system:masters"]
          },
        ], var.additional_aws_auth_roles)
      )
    )
  }

  depends_on = [
    module.eks.cluster_id,
    data.http.eks_cluster_readiness,
  ]
}

#################################################################################
## VPC-CNI Custom Networking ENIConfig
#################################################################################

resource "kubectl_manifest" "eni_config" {
  for_each = zipmap(var.availability_zones, slice(var.pod_subnet_ids, 0, length(var.availability_zones)))

  yaml_body = yamlencode({
    apiVersion = "crd.k8s.amazonaws.com/v1alpha1"
    kind       = "ENIConfig"
    metadata = {
      name = each.key
    }
    spec = {
      securityGroups = [
        module.eks.node_security_group_id,
      ]
      subnet = each.value
    }
  })

  depends_on = [
    module.eks,
    kubernetes_config_map.aws_auth,
  ]
}
