data "aws_availability_zones" "available" {}
data "aws_caller_identity" "current" {}
data "aws_partition" "current" {}

data "aws_eks_cluster_auth" "this" {
  name = module.eks.cluster_name
}

data "http" "eks_cluster_readiness" {
  url            = join("/", [module.eks.cluster_endpoint, "healthz"])
  ca_certificate = base64decode(module.eks.cluster_certificate_authority_data)
  timeout        = var.eks_readiness_timeout
}
