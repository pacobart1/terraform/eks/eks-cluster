locals {
  ec2_events = {
    health_event = {
      name        = "HealthEvent"
      description = "AWS health event"
      event_pattern = {
        source      = ["aws.health"]
        detail-type = ["AWS Health Event"]
      }
    }
    spot_interupt = {
      name        = "SpotInterrupt"
      description = "EC2 spot instance interruption warning"
      event_pattern = {
        source      = ["aws.ec2"]
        detail-type = ["EC2 Spot Instance Interruption Warning"]
      }
    }
    instance_rebalance = {
      name        = "InstanceRebalance"
      description = "EC2 instance rebalance recommendation"
      event_pattern = {
        source      = ["aws.ec2"]
        detail-type = ["EC2 Instance Rebalance Recommendation"]
      }
    }
    instance_state_change = {
      name        = "InstanceStateChange"
      description = "EC2 instance state-change notification"
      event_pattern = {
        source      = ["aws.ec2"]
        detail-type = ["EC2 Instance State-change Notification"]
      }
    }
  }
}

module "karpenter_sqs" {
  #checkov:skip=CKV_TF_1:modules are pinned to version in terraform cloud
  source  = "terraform-aws-modules/sqs/aws"
  version = "4.0.1"
  count   = var.enable_addons && var.enable_argocd ? 1 : 0

  name                              = "${module.eks.cluster_name}-karpenter"
  message_retention_seconds         = 300
  sqs_managed_sse_enabled           = true
  kms_master_key_id                 = "${module.eks.cluster_name}-karpenter-sqs" # NOTE: module.kms_karpenter_sqs[count.index].key_id causes cycle error
  kms_data_key_reuse_period_seconds = 300

  create_queue_policy = true
  queue_policy_statements = {
    account = {
      sid     = "SendEventsToQueue"
      actions = ["sqs:SendMessage"]
      principals = [
        {
          type = "Service"
          identifiers = [
            "events.${local.dns_suffix}",
            "sqs.${local.dns_suffix}",
          ]
        }
      ]
    }
  }

  tags = var.tags
}

module "kms_karpenter_sqs" {
  #checkov:skip=CKV_TF_1:modules are pinned to version in terraform cloud
  source      = "terraform-aws-modules/kms/aws"
  version     = "2.1.0"
  count       = var.enable_addons && var.enable_argocd ? 1 : 0
  aliases     = ["${module.eks.cluster_name}-karpenter-sqs"]
  description = "KMS key used for Karpenter's SQS queue"
  key_statements = [
    {
      sid       = "AllowKarpenterToRecieveFromTheQueue"
      actions   = ["kms:Decrypt"]
      resources = ["*"]
      principals = [
        {
          type        = "AWS"
          identifiers = [module.karpenter_pod_identity[count.index].iam_role_arn]
        },
      ]
    },
  ]
  tags = var.tags
}

resource "aws_cloudwatch_event_rule" "karpenter" {
  for_each = { for k, v in local.ec2_events : k => v if var.enable_addons && var.enable_argocd }

  name          = "${module.eks.cluster_name}-karpenter-${each.value.name}"
  description   = each.value.description
  event_pattern = jsonencode(each.value.event_pattern)

  tags = merge(
    {
      ClusterName = module.eks.cluster_name
    },
    var.tags,
  )
}

resource "aws_cloudwatch_event_target" "karpenter" {
  for_each = { for k, v in local.ec2_events : k => v if var.enable_addons && var.enable_argocd }

  rule      = aws_cloudwatch_event_rule.karpenter[each.key].name
  target_id = "KarpenterQueueTarget"
  arn       = module.karpenter_sqs[0].queue_arn
}
