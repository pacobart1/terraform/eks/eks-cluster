module "argocd" {
  source             = "./modules/argocd"
  count              = var.enable_addons && var.enable_argocd ? 1 : 0
  argocd_addons_repo = var.argocd_addons_repo
  aws_region         = var.aws_region
  aws_account_id     = local.aws_account_id
  kms_key_id         = module.eks.kms_key_id
  environment        = var.environment
  eks_context = {
    cluster_name      = module.eks.cluster_name
    cluster_endpoint  = module.eks.cluster_endpoint
    cluster_version   = module.eks.cluster_version
    oidc_provider_arn = module.eks.oidc_provider_arn
  }
  create_argocd_applications = var.create_argocd_applications
  domain_name_suffix         = var.route53_zone_domain_name
  argocd_helm_releases = {
    awsLoadBalancerController = {
      serviceAccount = {
        annotations = {
          "eks.amazonaws.com/role-arn" = module.aws_load_balancer_controller_pod_identity[count.index].iam_role_arn
        }
      }
    }
    externalDns = { # NOTE: does not support irsav2 yet. Need to pass serviceaccount annotation
      zoneIdFilters = [var.route53_zone_id]
      serviceAccount = {
        annotations = {
          "eks.amazonaws.com/role-arn" = module.external_dns_pod_identity[count.index].iam_role_arn
        }
      }

    },
    karpenter = {
      interruptionQueue = module.karpenter_sqs[count.index].queue_name
    },
  }
  tags = var.tags

  depends_on = [
    module.eks,
    kubernetes_storage_class.gp3-encrypted,
  ]
}
