# NOTE: VPC cni needs to use IRSAv1 due to it needing to be available before compute nodes can be provisioned. All other addons should use Pod Identity
# IRSAv1
module "vpc_cni_irsa" {
  #checkov:skip=CKV_TF_1:modules are pinned to version in terraform cloud
  source  = "terraform-aws-modules/iam/aws//modules/iam-role-for-service-accounts-eks"
  version = "~> 5.0"

  role_name             = "${module.eks.cluster_name}-vpc-cni-irsa"
  attach_vpc_cni_policy = true
  vpc_cni_enable_ipv6   = false
  role_policy_arns = {
    cni = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  }

  oidc_providers = {
    main = {
      provider_arn               = module.eks.oidc_provider_arn
      namespace_service_accounts = ["kube-system:aws-node"]
    }
  }

  tags = var.tags
}

# Pod Identity method - requires pod-identity-agent to be deploying in core addons
module "adot_collector_pod_identity" {
  source                          = "./modules/pod_identity"
  count                           = var.enable_addons && var.enable_argocd ? 1 : 0
  name                            = "adot-collector"
  cluster_name                    = module.eks.cluster_name
  kubernetes_namespace            = "amazon-metrics"
  kubernetes_service_account_name = "adot-collector-sa"
  attachment_policy_arns = [
    "arn:aws:iam::aws:policy/AmazonPrometheusRemoteWriteAccess",
    "arn:aws:iam::aws:policy/AWSXrayWriteOnlyAccess",
    "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"
  ]
  tags = var.tags
}

module "aws_load_balancer_controller_pod_identity" {
  source                              = "./modules/pod_identity"
  count                               = var.enable_addons && var.enable_argocd ? 1 : 0
  name                                = "aws-lb-controller"
  cluster_name                        = module.eks.cluster_name
  oidc_provider_arn                   = module.eks.oidc_provider_arn
  kubernetes_namespace                = "kube-system"
  kubernetes_service_account_name     = "aws-load-balancer-controller"
  create_eks_pod_identity_association = false
  enable_irsav1_trust_policy          = true # NOTE: doesn't support pod identity yet.
  additional_inline_policies = {
    aws_load_balancer_controller = data.aws_iam_policy_document.load_balancer_controller[count.index].json
  }
  tags = var.tags
}

module "external_dns_pod_identity" {
  source                              = "./modules/pod_identity"
  count                               = var.enable_addons && var.enable_argocd ? 1 : 0
  name                                = "external-dns"
  cluster_name                        = module.eks.cluster_name
  oidc_provider_arn                   = module.eks.oidc_provider_arn
  kubernetes_namespace                = "external-dns"
  kubernetes_service_account_name     = "external-dns"
  create_eks_pod_identity_association = false
  enable_irsav1_trust_policy          = true # NOTE: doesn't support pod identity yet. https://github.com/kubernetes-sigs/external-dns/issues/4083
  additional_inline_policies = {
    external_dns = data.aws_iam_policy_document.external_dns[count.index].json
  }
  tags = var.tags
}

module "karpenter_pod_identity" {
  source                          = "./modules/pod_identity"
  count                           = var.enable_addons && var.enable_argocd ? 1 : 0
  name                            = "karpenter"
  cluster_name                    = module.eks.cluster_name
  kubernetes_namespace            = "karpenter"
  kubernetes_service_account_name = "karpenter"
  additional_inline_policies = {
    karpenter = data.aws_iam_policy_document.karpenter_controller[count.index].json

  }
  tags = var.tags
}

module "aws_ebs_csi_driver_pod_identity" {
  source                          = "./modules/pod_identity"
  count                           = var.enable_addons && var.enable_argocd ? 1 : 0
  name                            = "ebs-csi-driver"
  cluster_name                    = module.eks.cluster_name
  kubernetes_namespace            = "kube-system"
  kubernetes_service_account_name = "ebs-csi-controller-sa"
  additional_inline_policies = {
    ebs_csi = data.aws_iam_policy_document.ebs_csi[count.index].json
  }
  tags = var.tags
}

module "aws_efs_csi_driver_pod_identity" {
  source                          = "./modules/pod_identity"
  count                           = var.enable_addons && var.enable_argocd ? 1 : 0
  name                            = "efs-csi-driver"
  cluster_name                    = module.eks.cluster_name
  kubernetes_namespace            = "kube-system"
  kubernetes_service_account_name = "efs-csi-controller-sa"
  additional_inline_policies = {
    efs_csi = data.aws_iam_policy_document.efs_csi[count.index].json
  }
  tags = var.tags
}