resource "kubernetes_storage_class" "gp3-encrypted" {
  count = 1
  metadata {
    name = "gp3-encrypted"
    annotations = {
      "storageclass.kubernetes.io/is-default-class" = "true"
    }
  }
  storage_provisioner = "ebs.csi.aws.com"
  parameters = {
    type                        = "gp3"
    "csi.storage.k8s.io/fstype" = "ext4"
    encrypted                   = "true"
    kmsKeyId                    = module.kms_ebs[count.index].key_arn
  }
  volume_binding_mode = "WaitForFirstConsumer"

  depends_on = [
    module.eks,
    #kubernetes_config_map.aws_auth
  ]
}

module "kms_ebs" {
  #checkov:skip=CKV_TF_1:modules are pinned to version in terraform cloud
  source      = "terraform-aws-modules/kms/aws"
  version     = "2.1.0"
  count       = 1
  aliases     = ["${module.eks.cluster_name}-ebs"]
  description = "KMS key used for provisioning ebs volumes in EKS"
  key_statements = [
    {
      sid = "Allow access for all principals in the account that are authorized"
      actions = [
        "kms:CreateGrant",
        "kms:Decrypt",
        "kms:DescribeKey",
        "kms:Encrypt",
        "kms:GenerateDataKey*",
        "kms:ReEncrypt*",
      ]
      effect    = "Allow"
      resources = ["*"]

      principals = [
        {
          type = "AWS"
          identifiers = [
            "arn:${data.aws_partition.current.id}:iam::${local.aws_account_id}:root"
          ]
        }
      ]

      conditions = [
        {
          test     = "StringEquals"
          variable = "kms:CallerAccount"
          values = [
            local.aws_account_id,
          ]
        },
        {
          test     = "StringEquals"
          variable = "kms:ViaService"
          values = [
            "eks.${var.aws_region}.amazonaws.com",
          ]
        },
      ]
    }
  ]
  tags = var.tags
}

# NOTE: might not need now that using irsa eks module
#resource "aws_iam_policy" "aws_ebs_csi_driver_kms_policy" {
#  count       = 1
#  name        = "${module.eks.cluster_name}-ebs-kms-access"
#  description = "Allow ebs csi driver to use kms key"
#
#  policy = data.aws_iam_policy_document.aws_ebs_csi_driver_kms[count.index].json
#}
#
#data "aws_iam_policy_document" "aws_ebs_csi_driver_kms" {
#  count = 1
#  statement {
#    sid       = ""
#    effect    = "Allow"
#    resources = [module.kms_ebs[count.index].key_arn]
#
#    actions = [
#      "kms:Encrypt",
#      "kms:Decrypt",
#      "kms:ReEncrypt*",
#      "kms:GenerateDataKey*",
#      "kms:DescribeKey",
#    ]
#  }
#
#  statement {
#    sid       = ""
#    effect    = "Allow"
#    resources = [module.kms_ebs[count.index].key_arn]
#
#    actions = [
#      "kms:CreateGrant",
#      "kms:ListGrants",
#      "kms:RevokeGrant"
#    ]
#
#    condition {
#      test     = "Bool"
#      variable = "kms:GrantIsForAWSResource"
#      values   = [true]
#    }
#  }
#}

resource "null_resource" "remove_default_storageclass" {
  triggers = {
    #always_run = timestamp()
  }

  provisioner "local-exec" {
    interpreter = ["/bin/sh", "-c"]

    # We are removing the default storageclass provided by the EKS service and replacing it with a gp3 encrypted default
    command = <<-EOT
      aws eks update-kubeconfig --name ${module.eks.cluster_name} --region ${var.aws_region}
      kubectl delete storageclass gp2 || true
    EOT
  }

  depends_on = [
    module.eks,
    kubernetes_storage_class.gp3-encrypted
  ]
}
