data "aws_iam_policy_document" "fis_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["fis.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "fis_policy" {
  statement {
    sid = "StartExperiment"
    actions = [
      "fis:StartExperiment"
    ]
    resources = [
      "arn:aws:fis:*:*:experiment-template/${local.name}-terminate-worker-nodes",
      "arn:aws:fis:*:*:experiment/*"
    ]
  }

  statement {
    sid = "PermissionsToCreateServiceLinkedRole"
    actions = [
      "iam:CreateServiceLinkedRole"
    ]
    resources = ["*"]
    condition {
      test     = "StringEquals"
      variable = "iam:AWSServiceName"
      values   = ["fis.amazonaws.com"]
    }
  }
}

resource "aws_iam_role_policy" "fis" {
  name   = "fis-actions"
  role   = aws_iam_role.fis.id
  policy = data.aws_iam_policy_document.fis_policy.json
}

resource "aws_iam_role" "fis" {
  name               = "${local.name}-fis"
  assume_role_policy = data.aws_iam_policy_document.fis_assume_role_policy.json
  tags               = var.tags
}

resource "aws_cloudwatch_metric_alarm" "fis_nodes_less_than_two" {
  alarm_name          = "${local.name}-nodes-less-than-two"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = 2
  metric_name         = "cluster_node_count"
  namespace           = "ContainerInsights"
  period              = 120
  statistic           = "Average"
  threshold           = 2
  alarm_description   = "This metric monitors amount of nodes available"
}

resource "aws_fis_experiment_template" "terminate_worker_nodes" {
  description = "${local.name}-terminate-worker-nodes"
  role_arn    = aws_iam_role.fis.arn

  stop_condition {
    source = "aws:cloudwatch:alarm"
    value  = aws_cloudwatch_metric_alarm.fis_nodes_less_than_two.arn
  }

  action {
    name      = "TerminateWorkerNode"
    action_id = "aws:eks:terminate-nodegroup-instances"

    target {
      key   = "Nodegroups"
      value = "Nodegroups-Target-1"
    }

    parameter {
      key   = "instanceTerminationPercentage"
      value = "40"
    }
  }

  target {
    name           = "Nodegroups-Target-1"
    resource_type  = "aws:eks:nodegroup"
    selection_mode = "ALL"

    resource_tag {
      key   = "Name"
      value = "base"
    }
  }
  tags = merge(
    var.tags,
    {
      Name = "${local.name}-terminate-worker-nodes"
  })
}
