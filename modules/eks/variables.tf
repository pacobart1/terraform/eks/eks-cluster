variable "name" {
  description = "name to use for everything"
}

variable "environment" {
  description = "deploy environment"
}

variable "aws_region" {
  description = "aws region"
  default     = "us-west-2"
}


### Network variables
variable "vpc_id" {
  description = "vpc id"
  type        = string
}

variable "control_plane_subnet_ids" {
  description = "eks control plane subnet ids"
  type        = list(string)
}

variable "worker_subnet_ids" {
  description = "eks worker subnet ids"
  type        = list(string)
}

variable "pod_subnet_ids" {
  description = "eks pod subnet ids for custom networking"
  type        = list(string)
}

variable "availability_zones" {
  description = "availability zones"
  type        = list(string)
}

### EKS Variables

variable "eks_cluster_version" {
  description = "eks cluster version number"
  default     = "1.28"
}

variable "cluster_endpoint_public_access" {
  description = "cluster to use public endpoint addresses. requried if trying to access outside of VPC without VPN"
  type        = bool
  default     = true
}

variable "cluster_endpoint_private_access" {
  description = "cluster to use private endpoint addresses."
  type        = bool
  default     = true
}

variable "cluster_endpoint_public_access_cidrs" {
  description = "List of CIDR blocks which can access the Amazon EKS public API server endpoint"
  type        = list(string)
  default     = ["0.0.0.0/0"]
}

variable "eks_readiness_timeout" {
  description = "The maximum time (in seconds) to wait for EKS API server endpoint to become healthy"
  type        = number
  default     = "600"
}

variable "cluster_ip_family" {
  description = "The IP family used to assign Kubernetes pod and service addresses. Valid values are `ipv4` (default) and `ipv6`. You can only specify an IP family when you create a cluster, changing this value will force a new cluster to be created"
  type        = string
  default     = null
}

variable "cluster_service_ipv4_cidr" {
  description = "The CIDR block to assign Kubernetes service IP addresses from. If you don't specify a block, Kubernetes assigns addresses from either the 10.100.0.0/16 or 172.20.0.0/16 CIDR blocks"
  type        = string
  default     = null
}

variable "eks_managed_node_group_defaults" {
  description = "Map of self-managed node group default configurations"
  type        = any
  default     = {}
}

variable "eks_managed_node_groups" {
  description = "eks managed node groups"
  type        = any
  default = {
    base = {
      k8s_labels = {}
      k8s_taints = {}
      tags       = {}
    }
  }
}

variable "node_security_group_additional_rules" {
  description = "List of additional security group rules to add to the node security group created. Set `source_cluster_security_group = true` inside rules to set the `cluster_security_group` as source"
  type        = any
  default     = {}
}

variable "node_security_group_enable_recommended_rules" {
  description = "Determines whether to enable recommended security group rules for the node security group created. This includes node-to-node TCP ingress on ephemeral ports and allows all egress traffic"
  type        = bool
  default     = true
}

variable "node_security_group_tags" {
  description = "A map of additional tags to add to the node security group created"
  type        = map(string)
  default     = {}
}

variable "route53_zone_domain_name" {
  description = "public route53 domain name"
  type        = string
}

variable "route53_zone_arn" {
  description = "public route53 zone arn"
  type        = string
}

variable "route53_zone_id" {
  description = "public route53 zone id"
  type        = string
}

variable "enable_addons" {
  description = "deploy eks addons and helm charts"
  type        = string
  default     = true
}

variable "enable_argocd" {
  description = "enable argocd to deploy cluster addons"
  type        = bool
  default     = true
}

variable "create_argocd_applications" {
  description = "create argocd applications"
  type        = bool
  default     = true
}

variable "argocd_addons_repo" {
  description = "git repository url for argocd addons"
  type        = string
}

variable "additional_aws_auth_roles" {
  description = "additional aws auth roles"
  type = list(object({
    rolearn  = string
    username = string
    groups   = list(string)
  }))
  default = []
}

variable "kms_key_administrators" {
  description = "iam user arns for kms key administrators"
  type        = list(string)
}

variable "tags" {
  description = "map of tags"
  type        = any
  default     = {}
}
