variable "name" {
  description = "name for resources"
  type        = string
}

variable "cluster_name" {
  description = "name of the eks cluster"
  type        = string
}

variable "kubernetes_namespace" {
  description = "kubernetes namespace"
  type        = string
}

variable "kubernetes_service_account_name" {
  description = "name of kubernetes service account to use iam role"
}

variable "create_eks_pod_identity_association" {
  description = "to create pod identity or not. used for compatibility with irsav1"
  type        = bool
  default     = true
}

variable "attachment_policy_arns" {
  description = "additional policies to attach to iam role"
  type        = list(string)
  default     = []
}

variable "additional_inline_policies" {
  description = "additional inline policies to attach to iam role"
  type        = any
  default     = {}
}

variable "tags" {
  description = "map of tags"
  type        = any
  default     = {}
}

variable "enable_irsav1_trust_policy" {
  description = "add trust policy on iam role for irsav1 service account support"
  type        = bool
  default     = false
}

variable "oidc_provider_arn" {
  description = "oidc provider arn used for irsav1 trust"
  type        = string
  default     = ""
}
