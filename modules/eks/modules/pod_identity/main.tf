resource "aws_iam_role" "pod_identity" {
  name               = "${var.cluster_name}-${var.name}"
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy.json
}

data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    sid    = "AllowEksAuthToAssumeRoleForPodIdentity"
    effect = "Allow"
    actions = [
      "sts:AssumeRole",
      "sts:TagSession"
    ]
    principals {
      type        = "Service"
      identifiers = ["pods.eks.amazonaws.com"]
    }
  }

  dynamic "statement" {
    for_each = var.enable_irsav1_trust_policy ? [1] : []
    content {
      sid    = "AllowIRSATrustRole"
      effect = "Allow"
      actions = [
        "sts:AssumeRoleWithWebIdentity"
      ]
      principals {
        type        = "Federated"
        identifiers = [var.oidc_provider_arn]
      }
      condition {
        test     = "StringEquals"
        variable = "${replace(var.oidc_provider_arn, "/^(.*provider/)/", "")}:sub"
        values   = ["system:serviceaccount:${var.kubernetes_namespace}:${var.kubernetes_service_account_name}"]
      }

      condition {
        test     = "StringEquals"
        variable = "${replace(var.oidc_provider_arn, "/^(.*provider/)/", "")}:aud"
        values   = ["sts.amazonaws.com"]
      }
    }
  }
}

resource "aws_iam_role_policy_attachment" "attachments" {
  for_each   = toset(var.attachment_policy_arns)
  policy_arn = each.value
  role       = aws_iam_role.pod_identity.name
}

resource "aws_eks_pod_identity_association" "association" {
  count           = var.create_eks_pod_identity_association ? 1 : 0
  cluster_name    = var.cluster_name
  namespace       = var.kubernetes_namespace
  service_account = var.kubernetes_service_account_name
  role_arn        = aws_iam_role.pod_identity.arn
  tags            = var.tags
}

resource "aws_iam_role_policy" "additional_inline_policies" {
  for_each = var.additional_inline_policies
  name     = each.key
  role     = aws_iam_role.pod_identity.id
  policy   = each.value
}
