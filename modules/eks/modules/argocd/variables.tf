variable "eks_context" {
  description = "map of eks variables"
  type        = map(string)
}

variable "argocd_helm_chart_version" {
  description = "helm chart version"
  type        = string
  default     = "5.42.1"
}

variable "argocd_addons_repo" {
  description = "git repository url for argocd addons"
  type        = string
}

variable "create_argocd_applications" {
  description = "create argocd applications"
  type        = bool
  default     = true
}

variable "aws_region" {
  description = "aws region"
}

variable "aws_account_id" {
  description = "aws account id"
  type        = string # NOTE: needs to be string as some account numbers start with zero and terraform will strip the prefix zero
}

variable "environment" {
  description = "deploy environment. sets the helm valuesfile"
  type        = string
}

variable "argocd_helm_releases" {
  description = "helm releases with parameters"
  # NOTE: don't define type due to the type of maps being unique
  #type        = map(object({}))
  default = {}
}

variable "kms_key_id" {
  description = "kms key id for encryting secretsmanager secret"
  type        = string
}

variable "tags" {
  description = "map of tags"
  type        = map(string)
  default     = {}
}

variable "domain_name_suffix" {
  description = "dns name suffix used for argocd. Ex: .mydomain.com"
  type        = string
}
