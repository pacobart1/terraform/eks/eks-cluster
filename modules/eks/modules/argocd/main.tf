locals {
  argocd_domain_name = "${var.eks_context.cluster_name}-argocd.${var.domain_name_suffix}"
}

#---------------------------------------------------------------
# ArgoCD Admin Password credentials with Secrets Manager
# Login to AWS Secrets manager with the same role as Terraform to extract the ArgoCD admin password with the secret name as "argocd"
#---------------------------------------------------------------
resource "random_password" "argocd" {
  length           = 16
  special          = true
  override_special = "!#$%&*()-_=+[]{}<>:?"
}

# Argo requires the password to be bcrypt, we use custom provider of bcrypt,
# as the default bcrypt function generates diff for each terraform plan
resource "bcrypt_hash" "argo" {
  cleartext = random_password.argocd.result
}

#tfsec:ignore:aws-ssm-secret-use-customer-key
resource "aws_secretsmanager_secret" "argocd" {
  #checkov:skip=CKV2_AWS_57:won't be rotating this secret until something like external-secrets is setup
  name                    = "${var.eks_context.cluster_name}-argocd"
  recovery_window_in_days = 0 # Set to zero for this example to force delete during Terraform destroy
  #kms_key_id              = var.kms_key_id # TODO: issue with using KMS key access. Need to resolve
}

resource "aws_secretsmanager_secret_version" "argocd" {
  secret_id     = aws_secretsmanager_secret.argocd.id
  secret_string = random_password.argocd.result
}

module "argocd" {
  #checkov:skip=CKV_TF_1:modules are pinned to version in terraform cloud
  source           = "aws-ia/eks-blueprints-addon/aws"
  version          = "1.1.1"
  create           = true
  name             = "argo-cd"
  description      = "A Helm chart to install the ArgoCD"
  namespace        = "argocd"
  create_namespace = true
  chart            = "argo-cd"
  chart_version    = var.argocd_helm_chart_version
  repository       = "https://argoproj.github.io/argo-helm"
  values           = []
  atomic           = true
  wait             = false
  set = [
    {
      name  = "fullnameOverride"
      value = "argocd"
    },
    # NOTE: use ingress-nginx ingress
    {
      name  = "server.ingress.enabled"
      value = "true"
    },
    {
      name  = "server.ingress.ingressClassName"
      value = "nginx"
    },
    {
      name  = "server.ingress.hosts[0]"
      value = local.argocd_domain_name
    },
    #{
    #  name  = "server.ingress.tls"
    #  value = "" # TODO: cert-manager certificate
    #},
    {
      name  = "server.service.annotations.external-dns\\.alpha\\.kubernetes\\.io/hostname"
      value = local.argocd_domain_name
    },
    {
      name  = "server.service.annotations.external-dns\\.alpha\\.kubernetes\\.io/ttl"
      value = 10
    },
    # NOTE: if you want a separate load balancer for argocd use the below annotations:
    #{
    #  name  = "server.service.type"
    #  value = "LoadBalancer"
    #},
    #{
    #  name  = "server.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-name"
    #  value = "${var.eks_context.cluster_name}-argocd"
    #},
    #{
    #  name  = "server.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-backend-protocol"
    #  value = "tcp"
    #},
    #{
    #  name  = "server.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-cross-zone-load-balancing-enabled"
    #  value = true
    #},
    #{
    #  name  = "server.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-scheme"
    #  value = "internet-facing"
    #},
    #{
    #  name  = "server.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-type"
    #  value = "external"
    #},
    #{
    #  name  = "server.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-nlb-target-type"
    #  value = "ip"
    #},
    #{
    #  name  = "server.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-ssl-ports"
    #  value = "https"
    #},
    #{
    #  name  = "server.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-target-group-attributes"
    #  value = "preserve_client_ip.enabled=true"
    #},
  ]
  set_sensitive = [
    {
      name  = "configs.secret.argocdServerAdminPassword"
      value = bcrypt_hash.argo.id
    }
  ]

  tags = var.tags
}

resource "kubectl_manifest" "argocd_applications" {
  count     = var.create_argocd_applications ? 1 : 0
  yaml_body = data.template_file.argco_addon_applications.rendered
  depends_on = [
    module.argocd
  ]
}

data "template_file" "argco_addon_applications" {
  template = file("${path.module}/templates/argo_addon_applications.yaml")
  vars = {
    giturl             = var.argocd_addons_repo
    region             = var.aws_region
    account            = var.aws_account_id
    clusterName        = var.eks_context.cluster_name
    environment        = var.environment
    domain_name_suffix = var.domain_name_suffix
    helm_releases      = indent(8, yamlencode(var.argocd_helm_releases))
  }
}
