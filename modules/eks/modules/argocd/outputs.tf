#output "argo_application_manifest" {
#  value = data.template_file.argco_addon_applications.rendered
#}

output "argocd_domain_name" {
  value = local.argocd_domain_name
}

output "argocd_secretsmanager_secret" {
  value = aws_secretsmanager_secret.argocd.id
}
