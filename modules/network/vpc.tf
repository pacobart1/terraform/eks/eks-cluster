module "vpc" {
  #checkov:skip=CKV_TF_1:modules are pinned to version in terraform cloud
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 5.4.0"

  name = var.name
  cidr = var.vpc_cidr

  secondary_cidr_blocks = var.secondary_cidr_blocks # can add up to 5 total CIDR blocks

  azs = var.availability_zones
  #private_subnets = concat(
  #  [for k, v in var.availability_zones : cidrsubnet(var.vpc_cidr, 4, k)],
  #  [for k, v in var.availability_zones : cidrsubnet(local.secondary_vpc_cidr, 2, k)] # NOTE: removed local.secondary_vpc_cidr
  #)
  private_subnets = [for k, v in var.availability_zones : cidrsubnet(var.vpc_cidr, 4, k)]
  public_subnets  = [for k, v in var.availability_zones : cidrsubnet(var.vpc_cidr, 8, k + 48)]
  #intra_subnets  = [for k, v in var.availability_zones : cidrsubnet(var.vpc_cidr, 8, k + 52)] # NOTE: removed local.secondary_vpc_cidr
  intra_subnets = [for k, v in var.availability_zones : cidrsubnet(element(var.secondary_cidr_blocks, 0), 2, k)] # NOTE: first index. No internet access subnets

  enable_nat_gateway = true
  single_nat_gateway = true

  public_subnet_tags = merge(var.public_subnet_tags, {
    Name = "${var.name}-public"
  })

  private_subnet_tags = merge(var.private_subnet_tags, {
    Name = "${var.name}-private"
  })

  intra_subnet_tags = merge(var.intra_subnet_tags, {
    Name = "${var.name}-intra"
  })

  default_route_table_tags = {
    Name = "${var.name}-default"
  }

  public_route_table_tags = {
    Name = "${var.name}-public"
  }

  private_route_table_tags = {
    Name = "${var.name}-private"
  }

  intra_route_table_tags = {
    Name = "${var.name}-intra"
  }

  tags = var.tags
}
