variable "name" {
  description = "name for resources created in the module"
  type        = string
}

variable "vpc_cidr" {
  description = "cidr for the vpc"
  type        = string
}

variable "secondary_cidr_blocks" {
  description = "secondary vpc cidr blocks. supports up to 5"
  default     = []
}

variable "enable_vpc_endpoints" {
  description = "enable vpc endpoints for private communication"
  type        = bool
  default     = true
}

variable "availability_zones" {
  description = "availability zones"
  type        = list(string)
}

variable "route53_zone_domain_name" {
  description = "route53 domain name"
  type        = string
}

variable "tags" {
  description = "map of tags"
  type        = any
  default     = {}
}

variable "public_subnet_tags" {
  description = "map of tags for public subnets"
  type        = any
  default     = {}
}

variable "private_subnet_tags" {
  description = "map of tags for private subnets"
  type        = any
  default     = {}
}

variable "intra_subnet_tags" {
  description = "map of tags for intra subnets"
  type        = any
  default     = {}
}
