resource "aws_route53_zone" "public" {
  #checkov:skip=CKV2_AWS_39:will configure query logging later
  name = var.route53_zone_domain_name

  force_destroy = true # ability to destroy records created using external-dns

  tags = var.tags

  depends_on = [
    module.vpc
  ]
}

#resource "aws_route53_key_signing_key" "public" {
#  hosted_zone_id             = aws_route53_zone.public.id
#  key_management_service_arn = aws_kms_key.dnssec.arn
#  name                       = var.route53_zone_domain_name
#  depends_on = [
#    aws_kms_key.dnssec
#  ]
#}
#
#resource "aws_route53_hosted_zone_dnssec" "public" {
#  hosted_zone_id = aws_route53_key_signing_key.public.hosted_zone_id
#  depends_on = [
#    aws_route53_key_signing_key.public
#  ]
#}
#
#resource "aws_kms_key" "dnssec" {
#  customer_master_key_spec = "ECC_NIST_P256"
#  deletion_window_in_days  = 7
#  key_usage                = "SIGN_VERIFY"
#  policy = jsonencode({
#    Statement = [
#      {
#        Action = [
#          "kms:DescribeKey",
#          "kms:GetPublicKey",
#          "kms:Sign",
#          "kms:Verify",
#        ],
#        Effect = "Allow"
#        Principal = {
#          Service = "dnssec-route53.amazonaws.com"
#        }
#        Resource = "*"
#        Sid      = "Allow Route 53 DNSSEC Service",
#      },
#      {
#        Action = "kms:*"
#        Effect = "Allow"
#        Principal = {
#          AWS = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"
#        }
#        Resource = "*"
#        Sid      = "Enable IAM User Permissions"
#      },
#    ]
#    Version = "2012-10-17"
#  })
#  tags = local.tags
#}
#
# TODO: enable route53 query logging
#resource "aws_route53_query_log" "public" {
#  depends_on = [aws_cloudwatch_log_resource_policy.route53-query-logging-policy]
#
#  cloudwatch_log_group_arn = aws_cloudwatch_log_group.aws_route53_example_com.arn
#  zone_id                  = aws_route53_zone.public.zone_id
#}
#
#module "kms_cloudwatch_route53_query_logging" {
#  #checkov:skip=CKV_TF_1:modules are pinned to version in terraform cloud
#  source = "terraform-aws-modules/kms/aws"
#
#  description = "encrypting cloudwatch logs"
#  key_usage   = "ENCRYPT_DECRYPT"
#
#  # Policy
#  key_administrators = local.kms_key_administrators
#
#  # Aliases
#  aliases = ["route53-query-logging"]
#
#  tags = local.tags
#}
#
#resource "aws_cloudwatch_log_group" "public" {
#  name              = "/aws/route53/${aws_route53_zone.public.name}"
#  retention_in_days = 30
#  kms_key_id        = module.kms_cloudwatch_route53_query_logging.key_arn
#}
#
#data "aws_iam_policy_document" "route53-query-logging-policy" {
#  statement {
#    actions = [
#      "logs:CreateLogStream",
#      "logs:PutLogEvents",
#    ]
#
#    resources = ["arn:aws:logs:*:*:log-group:/aws/route53/*"]
#
#    principals {
#      identifiers = ["route53.amazonaws.com"]
#      type        = "Service"
#    }
#  }
#}
#
#resource "aws_cloudwatch_log_resource_policy" "route53-query-logging-policy" {
#  policy_document = data.aws_iam_policy_document.route53-query-logging-policy.json
#  policy_name     = "route53-query-logging-policy"
#}
#