# NOTE: s3 gateway vpc endpoint needs to be provisioned before s3 interface endpoint.
module "vpc_endpoint_s3_gateway" {
  #checkov:skip=CKV_TF_1:modules are pinned to version in terraform cloud
  source  = "terraform-aws-modules/vpc/aws//modules/vpc-endpoints"
  version = "~> 5.0"

  count                 = var.enable_vpc_endpoints ? 1 : 0
  vpc_id                = module.vpc.vpc_id
  create_security_group = false
  #security_group_name_prefix = "${var.name}-vpc-endpoints-"
  #security_group_description = "VPC endpoint security group"
  security_group_rules = {
    ingress_https = {
      description = "HTTPS from VPC"
      cidr_blocks = concat([module.vpc.vpc_cidr_block], module.vpc.vpc_secondary_cidr_blocks)
    }
  }

  endpoints = {
    s3_gateway = {
      service             = "s3"
      service_type        = "Gateway"
      private_dns_enabled = true
      route_table_ids     = flatten([module.vpc.intra_route_table_ids, module.vpc.private_route_table_ids, module.vpc.public_route_table_ids])
      policy              = data.aws_iam_policy_document.generic_endpoint_policy.json
      tags                = merge(var.tags, { Name = "s3-gateway-vpc-endpoint" })
    },
  }

  tags = var.tags
}

module "vpc_endpoints" {
  #checkov:skip=CKV_TF_1:modules are pinned to version in terraform cloud
  source  = "terraform-aws-modules/vpc/aws//modules/vpc-endpoints"
  version = "~> 5.0"

  count                      = var.enable_vpc_endpoints ? 1 : 0
  vpc_id                     = module.vpc.vpc_id
  create_security_group      = true
  security_group_name_prefix = "${var.name}-vpc-endpoints-"
  security_group_description = "VPC endpoint security group"
  security_group_rules = {
    ingress_https = {
      description = "HTTPS from VPC"
      cidr_blocks = concat([module.vpc.vpc_cidr_block], module.vpc.vpc_secondary_cidr_blocks)
    }
  }

  endpoints = {
    s3 = {
      service             = "s3"
      private_dns_enabled = true
      subnet_ids          = module.vpc.private_subnets
      policy              = data.aws_iam_policy_document.generic_endpoint_policy.json
      tags                = merge(var.tags, { Name = "s3-vpc-endpoint" })
    },
    #s3_gateway = {
    #  service             = "s3"
    #  service_type        = "Gateway"
    #  private_dns_enabled = true
    #  route_table_ids     = flatten([module.vpc.intra_route_table_ids, module.vpc.private_route_table_ids, module.vpc.public_route_table_ids])
    #  policy              = data.aws_iam_policy_document.generic_endpoint_policy.json
    #  tags                = merge(var.tags, { Name = "s3-gateway-vpc-endpoint" })
    #},
    ecr_api = {
      service             = "ecr.api"
      private_dns_enabled = true
      subnet_ids          = module.vpc.private_subnets
      policy              = data.aws_iam_policy_document.generic_endpoint_policy.json
      tags                = merge(var.tags, { Name = "ecr-api-vpc-endpoint" })
    },
    ecr_dkr = {
      service             = "ecr.dkr"
      private_dns_enabled = true
      subnet_ids          = module.vpc.private_subnets
      policy              = data.aws_iam_policy_document.generic_endpoint_policy.json
      tags                = merge(var.tags, { Name = "ecr-dkr-vpc-endpoint" })
    },
    # TODO: validate eks endpoint
    eks = {
      service             = "eks"
      private_dns_enabled = true
      subnet_ids          = module.vpc.private_subnets
      # NOTE: Service com.amazonaws.us-west-2.eks only supports the full-access endpoint policy.
      #policy              = data.aws_iam_policy_document.generic_endpoint_policy.json
      tags = merge(var.tags, { Name = "eks-vpc-endpoint" })
    },
    eks-auth = {
      service             = "eks-auth"
      private_dns_enabled = true
      subnet_ids          = module.vpc.private_subnets
      policy              = data.aws_iam_policy_document.generic_endpoint_policy.json
      tags                = merge(var.tags, { Name = "eks-auth-vpc-endpoint" })
    },
    ec2 = {
      service             = "ec2"
      private_dns_enabled = true
      subnet_ids          = module.vpc.private_subnets
      policy              = data.aws_iam_policy_document.generic_endpoint_policy.json
      tags                = merge(var.tags, { Name = "ec2-vpc-endpoint" })
    },
    ec2messages = {
      service             = "ec2messages"
      private_dns_enabled = true
      subnet_ids          = module.vpc.private_subnets
      policy              = data.aws_iam_policy_document.generic_endpoint_policy.json
      tags                = merge(var.tags, { Name = "ec2messages-vpc-endpoint" })
    },
    ssm = {
      service             = "ssm"
      private_dns_enabled = true
      subnet_ids          = module.vpc.private_subnets
      policy              = data.aws_iam_policy_document.generic_endpoint_policy.json
      tags                = merge(var.tags, { Name = "ssm-vpc-endpoint" })
    },
    ssm_messages = {
      service             = "ssmmessages"
      private_dns_enabled = true
      subnet_ids          = module.vpc.private_subnets
      #policy              = data.aws_iam_policy_document.generic_endpoint_policy.json # TODO: ssmmessages:CreateDataChannel
      tags = merge(var.tags, { Name = "ssmmessages-vpc-endpoint" })
    },
    logs = {
      service             = "logs"
      private_dns_enabled = true
      subnet_ids          = module.vpc.private_subnets
      policy              = data.aws_iam_policy_document.generic_endpoint_policy.json
      tags                = merge(var.tags, { Name = "logs-vpc-endpoint" })
    },
    guardduty-data = {
      service             = "guardduty-data"
      private_dns_enabled = true
      subnet_ids          = module.vpc.private_subnets
      policy              = data.aws_iam_policy_document.generic_endpoint_policy.json
      tags                = merge(var.tags, { Name = "guardduty-data-vpc-endpoint" })
    },
  }

  tags = var.tags
  depends_on = [
    module.vpc_endpoint_s3_gateway,
  ]
}

data "aws_iam_policy_document" "generic_endpoint_policy" {
  statement {
    effect    = "Allow"
    actions   = ["*"]
    resources = ["*"]

    principals {
      type        = "*"
      identifiers = ["*"]
    }
  }
  # TODO: issue using this on all endpoints. need to troubleshoot.
  #statement {
  #  effect    = "Deny"
  #  actions   = ["*"]
  #  resources = ["*"]

  #  principals {
  #    type        = "*"
  #    identifiers = ["*"]
  #  }

  #  condition {
  #    test     = "StringNotEquals"
  #    variable = "aws:SourceVpc"

  #    values = [module.vpc.vpc_id]
  #  }
  #}
}
