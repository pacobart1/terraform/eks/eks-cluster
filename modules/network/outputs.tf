output "vpc_id" {
  value = module.vpc.vpc_id
}

output "vpc_intra_subnets" {
  value = module.vpc.intra_subnets
}

output "vpc_private_subnets" {
  value = module.vpc.private_subnets
}

output "route53_zone_arn" {
  value = aws_route53_zone.public.arn
}

output "route53_zone_id" {
  value = aws_route53_zone.public.zone_id
}