terraform {
  required_version = ">= 1.6.6"

  #backend "http" {}
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "awspbb"
    token        = ""
    workspaces {
      name = "devops-eks"
    }
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.29.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.24.0"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.14"
    }
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.4.1"
    }
    http = {
      source  = "terraform-aws-modules/http"
      version = "2.4.1"
    }
    bcrypt = {
      source  = "viktorradnai/bcrypt"
      version = ">= 0.1.2"
    }
  }
}

provider "aws" {
  region = local.region
  default_tags {
    tags = {
      managed_by = "terraform"
    }
  }
}

locals {
  eks_endpoint                       = var.deploy_eks ? module.eks[0].cluster_endpoint : ""
  cluster_certificate_authority_data = var.deploy_eks ? module.eks[0].cluster_certificate_authority_data : ""
  cluster_ca_certificate             = var.deploy_eks ? base64decode(local.cluster_certificate_authority_data) : ""
  eks_token                          = var.deploy_eks ? data.aws_eks_cluster_auth.this[0].token : ""
}

provider "kubernetes" {
  host                   = local.eks_endpoint
  cluster_ca_certificate = local.cluster_ca_certificate
  token                  = local.eks_token
  #exec {
  #  api_version = "client.authentication.k8s.io/v1beta1"
  #  command     = "aws"
  #  args        = ["eks", "get-token", "--cluster-name", var.name]
  #}
}

provider "kubectl" {
  apply_retry_count      = 10
  host                   = local.eks_endpoint
  cluster_ca_certificate = local.cluster_ca_certificate
  load_config_file       = false
  token                  = local.eks_token
  #exec {
  #  api_version = "client.authentication.k8s.io/v1beta1"
  #  command     = "aws"
  #  args        = ["eks", "get-token", "--cluster-name", var.name]
  #}
}

provider "helm" {
  kubernetes {
    host                   = local.eks_endpoint
    cluster_ca_certificate = local.cluster_ca_certificate
    token                  = local.eks_token
    #exec {
    #  api_version = "client.authentication.k8s.io/v1beta1"
    #  command     = "aws"
    #  args        = ["eks", "get-token", "--cluster-name", var.name]
    #}
  }
}
