# devops-eks

## TODO

- [x] Create VPC
- [x] Create EKS Cluster
- [x] EKS addon: external-dns
- [x] EKS addon: aws-load-balancer-controller
- [x] EKS addon: ingress-nginx
- [x] EKS addon: metrics-server
- [ ] EKS addon: karpenter
- [ ] EKS addon: cert-manager
- [ ] EKS addon: istio
- [x] EKS addon: gatekeeper
- [ ] EKS addon: ADOT
- [ ] EKS addon: kubecost
- [x] Network Policy enablement
- [ ] Network recommened policies
- [ ] KMS Encryption for contol plane secrets and ebs volumes (including karpenter)
- [ ] Cluster tests
- [ ] Synthetic test in CloudWatch
- [ ] Cloudwatch Dashboards
- [ ] Observability - prometheus, grafana, adot
- [x] GuardDuty
- [ ] Gatekeeper recommended policies https://github.com/open-policy-agent/gatekeeper-library/tree/master/artifacthub/library/general
- [x] Fault Injection Simulator
- [ ] Custom AMI - EC2 Image Builder
- [ ] Auotmated Node AMI replacement every 7 days
- [ ] Sample application (ingress, service, pod, hpa)
- [ ] load test (siege) `siege -c 200 -i <dns>/`
- [ ] Sentinel Policies
- [ ] Cleanup code for reusability
- [ ] Blue/Green cluster with ALB target groups and/or dns cutover
- [ ] Migrate IRSA to `aws_eks_pod_identity_association` pattern
- [ ] Replace aws-auth configmap with new access: https://docs.aws.amazon.com/eks/latest/userguide/access-entries.html

## Architecture

## Features

TODO: add features and callouts to the architecture.


### Fault Injection Simulator

This codebase will deploy an experiment one can run to simulate node terminations.
## Setup

Need to create iam role in aws account for gitlab pipeline to assume:

trust relationship:
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::979517299116:role/gitlab-runners-prod"
            },
            "Action": [
                "sts:TagSession",
                "sts:AssumeRole"
            ],
            "Condition": {
                "StringEquals": {
                    "aws:PrincipalTag/GitLab:Group": "pbb",
                    "aws:PrincipalTag/GitLab:Project": [
                        "devops-eks"
                    ]
                }
            }
        }
    ]
}
```

iam policies:
```

```

## Known Issues

On destroy, ingress-nginx controller doesn't delete the load balancer before the eks cluster, leaving the resource behind. Need to solve for this.

## Troubleshooting

If getting `unauthorized` when using kubectl:

CloudWatch --> Logs Insights --> Select cluster and use the following query:

```
fields @logstream, @timestamp, @message
| sort @timestamp desc
| filter @logStream like /authenticator/
| filter @message like "access denied"
| limit 20
```
<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.5.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 5 |
| <a name="requirement_helm"></a> [helm](#requirement\_helm) | >= 2.4.1 |
| <a name="requirement_http"></a> [http](#requirement\_http) | 2.4.1 |
| <a name="requirement_kubectl"></a> [kubectl](#requirement\_kubectl) | >= 1.14 |
| <a name="requirement_kubernetes"></a> [kubernetes](#requirement\_kubernetes) | >= 2.18.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 5 |
| <a name="provider_kubectl"></a> [kubectl](#provider\_kubectl) | >= 1.14 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_adot_collector_irsa"></a> [adot\_collector\_irsa](#module\_adot\_collector\_irsa) | terraform-aws-modules/iam/aws//modules/iam-role-for-service-accounts-eks | ~> 5.0 |
| <a name="module_eks"></a> [eks](#module\_eks) | terraform-aws-modules/eks/aws | ~> 19.16 |
| <a name="module_eks_blueprints_addons"></a> [eks\_blueprints\_addons](#module\_eks\_blueprints\_addons) | aws-ia/eks-blueprints-addons/aws | 1.9.2 |
| <a name="module_vpc"></a> [vpc](#module\_vpc) | terraform-aws-modules/vpc/aws | ~> 5.0 |
| <a name="module_vpc_cni_irsa"></a> [vpc\_cni\_irsa](#module\_vpc\_cni\_irsa) | terraform-aws-modules/iam/aws//modules/iam-role-for-service-accounts-eks | ~> 5.0 |

## Resources

| Name | Type |
|------|------|
| [aws_cloudwatch_metric_alarm.fis_nodes_less_than_two](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_metric_alarm) | resource |
| [aws_fis_experiment_template.terminate_worker_nodes](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/fis_experiment_template) | resource |
| [aws_iam_role.fis](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy.fis](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_route53_zone.public](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_zone) | resource |
| [kubectl_manifest.eni_config](https://registry.terraform.io/providers/gavinbunney/kubectl/latest/docs/resources/manifest) | resource |
| [aws_availability_zones.available](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/availability_zones) | data source |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_eks_cluster_auth.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/eks_cluster_auth) | data source |
| [aws_iam_policy_document.fis_assume_role_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.fis_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | aws region | `string` | `"us-west-2"` | no |
| <a name="input_cluster_endpoint_public_access_cidrs"></a> [cluster\_endpoint\_public\_access\_cidrs](#input\_cluster\_endpoint\_public\_access\_cidrs) | List of CIDR blocks which can access the Amazon EKS public API server endpoint | `list(string)` | <pre>[<br>  "0.0.0.0/0"<br>]</pre> | no |
| <a name="input_eks_cluster_version"></a> [eks\_cluster\_version](#input\_eks\_cluster\_version) | eks cluster version number | `string` | `"1.25"` | no |
| <a name="input_eks_readiness_timeout"></a> [eks\_readiness\_timeout](#input\_eks\_readiness\_timeout) | The maximum time (in seconds) to wait for EKS API server endpoint to become healthy | `number` | `"600"` | no |
| <a name="input_name"></a> [name](#input\_name) | name to use for everything | `string` | `"devops-eks"` | no |
| <a name="input_route53_zone_domain_name"></a> [route53\_zone\_domain\_name](#input\_route53\_zone\_domain\_name) | public route53 domain name | `string` | `"devopseks.lab"` | no |
| <a name="input_secondary_cidr_blocks"></a> [secondary\_cidr\_blocks](#input\_secondary\_cidr\_blocks) | secondary vpc cidr blocks. supports up to 5 | `list` | <pre>[<br>  "100.64.0.0/16"<br>]</pre> | no |
| <a name="input_vpc_cidr"></a> [vpc\_cidr](#input\_vpc\_cidr) | vpc cidr | `string` | `"10.101.0.0/16"` | no |

## Outputs

No outputs.
<!-- END_TF_DOCS -->