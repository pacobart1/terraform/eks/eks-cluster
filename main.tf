locals {
  name           = "${var.name}-${var.environment}"
  region         = var.aws_region
  azs            = slice(data.aws_availability_zones.available.names, 0, 3)
  aws_account_id = data.aws_caller_identity.current.account_id
  tags = {
    Name        = local.name
    environment = var.environment
  }
}

module "network" {
  source                   = "./modules/network"
  count                    = 1
  name                     = local.name
  vpc_cidr                 = var.vpc_cidr
  secondary_cidr_blocks    = var.secondary_cidr_blocks
  enable_vpc_endpoints     = var.enable_vpc_endpoints
  availability_zones       = local.azs
  route53_zone_domain_name = var.route53_zone_domain_name
  tags                     = local.tags
  public_subnet_tags = {
    "kubernetes.io/role/elb"                = 1
    "kubernetes.io/cluster/${local.name}-1" = "shared"
  }
  private_subnet_tags = {
    "kubernetes.io/role/internal-elb"       = 1
    "kubernetes.io/cluster/${local.name}-1" = "shared"
  }
  intra_subnet_tags = {
    "kubernetes.io/cluster/${local.name}-1" = "shared"
  }
}

module "eks" {
  source                   = "./modules/eks"
  count                    = var.deploy_eks ? 1 : 0
  name                     = "${local.name}-1"
  environment              = var.environment
  vpc_id                   = module.network[count.index].vpc_id
  control_plane_subnet_ids = module.network[count.index].vpc_intra_subnets
  worker_subnet_ids        = slice(module.network[count.index].vpc_private_subnets, 0, 3) # We only want to assign the 10.0.* range subnets to the data plane
  pod_subnet_ids           = module.network[count.index].vpc_intra_subnets
  availability_zones       = local.azs
  additional_aws_auth_roles = [
    {
      rolearn  = "arn:aws:sts::${local.aws_account_id}:role/automation_pipeline_role"
      username = "automation"
      groups   = ["system:masters"]
    },
    {
      rolearn  = "arn:aws:iam::${local.aws_account_id}:role/Admin",
      username = "AdminRole"
      groups   = ["system:masters"]
    },

  ]
  kms_key_administrators = [
    "arn:aws:iam::573956063148:role/automation_pipeline_role",
    "arn:aws:iam::${local.aws_account_id}:role/Admin" # NOTE: needed for local terraform execution]
  ]
  enable_addons              = var.enable_addons
  enable_argocd              = var.enable_argocd
  create_argocd_applications = var.create_argocd_applications
  argocd_addons_repo         = var.argocd_addons_repo
  route53_zone_domain_name   = var.route53_zone_domain_name
  route53_zone_arn           = module.network[count.index].route53_zone_arn
  route53_zone_id            = module.network[count.index].route53_zone_id
  tags                       = local.tags

  # NOTE: depends_on causes issues with `known after apply` error.
  #depends_on = [
  #  module.network
  #]
}
