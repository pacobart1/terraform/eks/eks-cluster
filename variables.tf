variable "name" {
  description = "name to use for everything"
  default     = "devops-eks"
}

variable "environment" {
  description = "deploy environment"
  default     = "dev"
}

variable "aws_region" {
  description = "aws region"
  default     = "us-west-2"
}

### VPC Variables
variable "vpc_cidr" {
  description = "vpc cidr"
  default     = "10.101.0.0/16"
}

variable "secondary_cidr_blocks" {
  description = "secondary vpc cidr blocks. supports up to 5"
  default     = ["100.64.0.0/16"]
}

variable "enable_vpc_endpoints" {
  description = "enable vpc endpoints for private communication"
  type        = bool
  default     = true
}

### EKS Variables

variable "deploy_eks" {
  description = "deploy eks"
  type        = bool
  default     = true
}
variable "eks_cluster_version" {
  description = "eks cluster version number"
  default     = "1.28"
}

variable "cluster_endpoint_public_access" {
  description = "cluster to use public endpoint addresses. requried if trying to access outside of VPC without VPN"
  type        = bool
  default     = true
}

variable "cluster_endpoint_private_access" {
  description = "cluster to use private endpoint addresses."
  type        = bool
  default     = true
}

variable "cluster_endpoint_public_access_cidrs" {
  description = "List of CIDR blocks which can access the Amazon EKS public API server endpoint"
  type        = list(string)
  default     = ["0.0.0.0/0"]
}

variable "eks_readiness_timeout" {
  description = "The maximum time (in seconds) to wait for EKS API server endpoint to become healthy"
  type        = number
  default     = "600"
}

variable "route53_zone_domain_name" {
  description = "public route53 domain name"
  type        = string
  default     = "devopseks.lab"
}

variable "enable_addons" {
  description = "deploy eks addons and helm charts"
  type        = string
  default     = true
}

variable "enable_argocd" {
  description = "enable argocd to deploy cluster addons"
  type        = bool
  default     = true
}

variable "create_argocd_applications" {
  description = "create argocd applications"
  type        = bool
  default     = true
}

variable "argocd_addons_repo" {
  description = "git repository url for argocd addons"
  type        = string
  default     = "https://github.com/Pacobart/argo-eks-addons.git"
}
