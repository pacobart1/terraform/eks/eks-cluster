data "aws_caller_identity" "current" {}
data "aws_availability_zones" "available" {}

data "aws_eks_cluster_auth" "this" {
  count = var.deploy_eks ? 1 : 0
  name  = module.eks[0].cluster_name
}